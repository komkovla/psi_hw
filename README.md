# Python server

This is homework project for course **BI-PSI**.

It's an CLI app, for multithreaded robot communication and navigation with server using Python and SocketServer.

Used technologies:
- Python 3 - codeBase
- SocketServer - server framework
