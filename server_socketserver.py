# echo_server.py
import socket
import socketserver
import string
from collections import namedtuple
from locale import atoi
from parse import parse
from enum import Enum
HOST = "127.0.0.1"
PORT = 2034
TIMEOUT = 1
TIMEOUT_RECHARGING = 5
ENDIAN = 'BIG'
Key_table = namedtuple('Key_table', 'server client')
Robot_loc = namedtuple('Robot_loc', 'x y')
class Robot_move(Enum):
    MOVE = 1
    LEFT = 2
    RIGHT = 3
class Robot_dir(Enum):
    UP = 1
    LEFT = 2
    DOWN = 3
    RIGHT = 4
    SAME = 5
def count_hash(str) -> int:
    sum = 0
    for ch in str:
        sum += ord(ch)
    return (sum * 1000) % 65536

def get_dir(robot_loc_old, robot_loc):
    if robot_loc_old.x > robot_loc.x:
        return Robot_dir.LEFT
    if robot_loc_old.x < robot_loc.x:
        return Robot_dir.RIGHT
    if robot_loc_old.y < robot_loc.y:
        return Robot_dir.UP
    if robot_loc_old.y > robot_loc.y:
        return Robot_dir.DOWN
    return Robot_dir.SAME
def finished(robot_loc):
    return robot_loc.x == 0 and robot_loc.y == 0

class Robot():
    loc = Robot_loc
    loc_old = Robot_loc
    dir = Robot_dir.SAME
    last_move = Robot_move.MOVE
    kvadr = -1
    def step_forward(self, new_loc):
        self.loc_old = self.loc
        self.loc = new_loc
        print(f"loc =     {self.loc.x}, {self.loc.y}")
        # print(f"loc old = {self.loc_old.x}, {self.loc_old.y}")
        self.last_move = Robot_move.MOVE
        self.setDir()
        if self.loc.x >= 0 and self.loc.y >= 0:
            self.kvadr = 1
        elif self.loc.x <= 0 and self.loc.y >= 0:
            self.kvadr = 2
        elif self.loc.x <= 0 and self.loc.y <= 0:
            self.kvadr = 3
        elif self.loc.x >= 0 and self.loc.y <= 0:
            self.kvadr = 4
    def turn_left(self):
        if self.dir == Robot_dir.LEFT:
            self.dir = Robot_dir.DOWN
        if self.dir == Robot_dir.DOWN:
            self.dir = Robot_dir.RIGHT
        if self.dir == Robot_dir.RIGHT:
            self.dir = Robot_dir.UP
        if self.dir == Robot_dir.UP:
            self.dir = Robot_dir.LEFT
    def turn_right(self):
        if self.dir == Robot_dir.LEFT:
            self.dir = Robot_dir.UP
        if self.dir == Robot_dir.DOWN:
            self.dir = Robot_dir.LEFT
        if self.dir == Robot_dir.RIGHT:
            self.dir = Robot_dir.DOWN
        if self.dir == Robot_dir.UP:
            self.dir = Robot_dir.RIGHT
    def is_block(self):
        return self.loc_old.x == self.loc.x and self.loc_old.y == self.loc.y and self.last_move == Robot_move.MOVE
    def is_finish(self):
        if self.loc.x == 0 and self.loc.y == 0:
            return True
        return False
    def setDir(self):
        if self.loc_old.x > self.loc.x:
            self.dir = Robot_dir.LEFT
        elif self.loc_old.x < self.loc.x:
            self.dir = Robot_dir.RIGHT
        elif self.loc_old.y < self.loc.y:
            self.dir = Robot_dir.UP
        elif self.loc_old.y > self.loc.y:
            self.dir = Robot_dir.DOWN
        else:
            self.dir = Robot_dir.SAME
class MyTCPSocketHandler(socketserver.StreamRequestHandler):
    recharging = False
    recharging_timeout_count = 0
    key_table = [Key_table(23019, 32037), Key_table(32037, 29295), Key_table(18789, 13603), Key_table(16443, 29533),
                 Key_table(18189, 21952)]

    def handle(self):
        self.request.settimeout(1)
        try:
            if not self.auth():
                return
        except socket.timeout:
            print("TIMEOUT")
            return
        if not self.navigation():
            #self.SERVER_SYNTAX_ERROR()
            return
        self.SERVER_PICK_UP()
        message = self.req_untill_end(100)
        if message == "LOGIC_ERROR":
            return
        if not message:
           self.SERVER_SYNTAX_ERROR()
           return
        self.SERVER_LOGOUT()

    def auth(self):
        username = self.req_untill_end(20)
        if not username:
            self.SERVER_SYNTAX_ERROR()
            return False
        print(f"username = \"{username}\"")
        self.SERVER_KEY_REQUEST()
        key_id = self.get_number(5)
        if key_id == -1:
            self.SERVER_SYNTAX_ERROR()
            return False
        if key_id not in range(-1, 5):
            self.SERVER_KEY_OUT_OF_RANGE_ERROR()
            return False
        print(f"key_id = {key_id}")
        robot_hash = count_hash(username)
        hash_plus_server_key = (robot_hash + self.key_table[key_id].server) % 65536

        self.SERVER_CONFIRMATION(hash_plus_server_key)
        robot_hash_confirm = self.get_number(7)
        if robot_hash_confirm == -1:
            self.SERVER_SYNTAX_ERROR()
            return False
        hash_plus_klient_key = (robot_hash + self.key_table[key_id].client) % 65536
        if robot_hash_confirm != hash_plus_klient_key:
            self.SERVER_LOGIN_FAILED()
            return False
        self.SERVER_OK()
        return True

    def get_loc(self):
        client_ok = self.req_untill_end(12)
        if client_ok == "LOGIC ERROR":
            return False, Robot_loc(0,0)
        parse_res = parse("OK {:d} {:d}", client_ok, case_sensitive=True)
        try:
            robot_loc = Robot_loc(parse_res[0], parse_res[1])
        except TypeError:
            self.SERVER_SYNTAX_ERROR()
            return False, Robot_loc(0,0)
        return True, robot_loc



    def navigation(self):
        robot = Robot()
        robot.last_move = self.SERVER_TURN_LEFT()
        valid, robot.loc = self.get_loc()
        if not valid:
            return False
        if robot.is_finish():
            return True
        robot.last_move = self.SERVER_MOVE()
        valid, tmp_loc = self.get_loc()
        if not valid:
            return False
        robot.step_forward(tmp_loc)
        if robot.is_finish():
            return True
        robot.setDir()
        if robot.is_block():
            self.SERVER_TURN_RIGHT()
            valid, tmp_loc = self.get_loc()
            self.SERVER_MOVE()
            valid, tmp_loc = self.get_loc()
            if not valid:
                return False
            robot.step_forward(tmp_loc)
        robot.setDir()
        print("robot dir = ", robot.dir)
        # ------------------------------ ABOVE X -------------------------------- #
        if robot.kvadr == 1 or robot.kvadr == 2:
            if robot.dir == Robot_dir.LEFT:
                robot.last_move = self.SERVER_TURN_LEFT()
                valid, tmp_loc = self.get_loc()
                robot.turn_left()
            elif robot.dir == Robot_dir.RIGHT:
                robot.last_move = self.SERVER_TURN_RIGHT()
                valid, tmp_loc = self.get_loc()
                robot.turn_right()
            elif robot.dir == Robot_dir.UP:
                robot.last_move = self.SERVER_TURN_LEFT()
                valid, tmp_loc = self.get_loc()
                robot.turn_left()
                robot.last_move = self.SERVER_TURN_LEFT()
                valid, tmp_loc = self.get_loc()
                robot.turn_left()
            while True:
                if robot.is_finish():
                    return True
                if robot.loc.y == 0:
                    break
                robot.last_move = self.SERVER_MOVE()
                valid, tmp_loc = self.get_loc()
                if not valid:
                    return False
                robot.step_forward(tmp_loc)
                if robot.is_block():
                    robot.last_move = self.SERVER_TURN_LEFT()
                    valid, tmp_loc = self.get_loc()
                    robot.turn_left()
                    robot.last_move = self.SERVER_MOVE()
                    valid, tmp_loc = self.get_loc()
                    if not valid:
                        return False
                    robot.step_forward(tmp_loc)
                    robot.last_move = self.SERVER_TURN_RIGHT()
                    valid, tmp_loc = self.get_loc()
                    robot.turn_right()

        # ------------------------------ UNDER X -------------------------------- #
        else:
            print(f"robot kvadr: {robot.kvadr}\n")
            print(f"robot direction: {robot.dir}\n")
            if robot.dir == Robot_dir.LEFT:
                robot.last_move = self.SERVER_TURN_RIGHT()
                valid, tmp_loc = self.get_loc()
                robot.turn_right()
            elif robot.dir == Robot_dir.RIGHT:
                robot.last_move = self.SERVER_TURN_LEFT()
                valid, tmp_loc = self.get_loc()
                robot.turn_left()
            elif robot.dir == Robot_dir.DOWN:
                robot.last_move = self.SERVER_TURN_LEFT()
                valid, tmp_loc = self.get_loc()
                robot.turn_left()
                robot.last_move = self.SERVER_TURN_LEFT()
                valid, tmp_loc = self.get_loc()
                robot.turn_left()
            while True:
                robot.last_move = self.SERVER_MOVE()
                valid, tmp_loc = self.get_loc()
                if not valid:
                    return False
                robot.step_forward(tmp_loc)
                if robot.is_block():
                    if robot.loc.x > 0:
                        robot.last_move = self.SERVER_TURN_LEFT()
                        robot.turn_left()
                    else:
                        robot.last_move = self.SERVER_TURN_RIGHT()
                        robot.turn_right()

                    valid, tmp_loc = self.get_loc()

                    robot.last_move = self.SERVER_MOVE()
                    valid, tmp_loc = self.get_loc()
                    if not valid:
                        return False
                    robot.step_forward(tmp_loc)
                    if robot.loc.x > 0:
                        robot.last_move = self.SERVER_TURN_RIGHT()
                        robot.turn_right()
                    else:
                        robot.last_move = self.SERVER_TURN_LEFT()
                        robot.turn_left()
                    valid, tmp_loc = self.get_loc()

                if robot.is_finish():
                    return True
                if robot.loc.y == 0:
                    break
        if robot.loc.x < 0:
            if robot.dir == Robot_dir.UP:
                robot.last_move = self.SERVER_TURN_RIGHT()
                valid, tmp_loc = self.get_loc()
                robot.turn_right()
            elif robot.dir == Robot_dir.DOWN:
                robot.last_move = self.SERVER_TURN_LEFT()
                valid, tmp_loc = self.get_loc()
                robot.turn_left()
        elif robot.loc.x > 0:
            if robot.dir == Robot_dir.UP:
                robot.last_move = self.SERVER_TURN_LEFT()
                valid, tmp_loc = self.get_loc()
                robot.turn_left()
            elif robot.dir == Robot_dir.DOWN:
                robot.last_move = self.SERVER_TURN_RIGHT()
                valid, tmp_loc = self.get_loc()
                robot.turn_right()
        # ------------------------------ nav to 0 in x -------------------------------- #
        while True:
            if robot.is_finish():
                return True
            robot.last_move = self.SERVER_MOVE()
            valid, tmp_loc = self.get_loc()
            if not valid:
                return False
            robot.step_forward(tmp_loc)

            if robot.is_block():
                print("BLOCK")
                robot.last_move = self.SERVER_TURN_LEFT()
                valid, tmp_loc = self.get_loc()
                robot.last_move = self.SERVER_MOVE()
                valid, tmp_loc = self.get_loc()
                if not valid:
                    return False
                robot.last_move = self.SERVER_TURN_RIGHT()
                valid, tmp_loc = self.get_loc()
                robot.last_move = self.SERVER_MOVE()
                valid, tmp_loc = self.get_loc()
                if not valid:
                    return False
                robot.last_move = self.SERVER_MOVE()
                valid, tmp_loc = self.get_loc()
                if not valid:
                    return False
                robot.last_move = self.SERVER_TURN_RIGHT()
                valid, tmp_loc = self.get_loc()
                robot.last_move = self.SERVER_MOVE()
                valid, tmp_loc = self.get_loc()
                if not valid:
                    return False
                robot.step_forward(tmp_loc)
                robot.last_move = self.SERVER_TURN_LEFT()
                valid, tmp_loc = self.get_loc()
                robot.turn_left()

    def wait_recharge(self, mess):
        self.request.settimeout(5)
        res = self.req_untill_end(12)
        if res != "FULL POWER":
            return False
        self.request.settimeout(1)
        return True

    def wait_recharge_substring(self, substing):
        read_len = len("RECHARGING\a\b") - len(substing)
        end = self.req_untill_end(read_len+2)
        print(end)
        if not end:
            return False
        self.request.settimeout(5)
        res = self.req_untill_end(12)
        print("res = ", res)
        if res != "FULL POWER":
            return False
        self.request.settimeout(1)
        return True


    def req_untill_end(self, max_char):
        res = ""
        count = 0
        while count < max_char-1:
            ch1 = self.req_char()
            if not ch1:
                return ""
            count += 1
            if ch1 == '\a':
                ch2 = self.req_char()
                if not ch2:
                    return ""
                count += 1
                if ch2 == '\b':
                    if res == "RECHARGING":
                        if not self.wait_recharge(res):
                            self.SERVER_LOGIC_ERROR()
                            return "LOGIC_ERROR"
                        return self.req_untill_end(max_char)
                    return res
                else:
                    res+= ch1
                    res+= ch2
            else:
                res += ch1
        if res in "RECHARGING\a\b":
            print(f"recharging substring({res})")
            if not self.wait_recharge_substring(res):
                self.SERVER_LOGIC_ERROR()
                return "LOGIC_ERROR"
            return self.req_untill_end(max_char)
        return ""


    def req_char(self):
        return self.request.recv(1).decode()

    def get_number(self, max_len):
        id = -1
        id_str = self.req_untill_end(max_len)
        print(f"id string = \"{id_str}\"")
        parse_res = parse("{:d}", id_str)
        try:
            id = parse_res[0]
        except TypeError:
            return -1
        return id

    def check_end(self) -> bool:
        ch1 = self.req_char()
        if ch1 == '\a':
            ch2 = self.req_char()
            if ch2 == '\b':
                return True
        return False

    def SERVER_CONFIRMATION(self, number):
        self.request.sendall(f"{number}\a\b".encode())
    def SERVER_MOVE(self):
        self.request.sendall("102 MOVE\a\b".encode())
        return Robot_move.MOVE
    def SERVER_TURN_LEFT(self):
        self.request.sendall("103 TURN LEFT\a\b".encode())
        return Robot_move.LEFT
    def SERVER_TURN_RIGHT(self):
        self.request.sendall("104 TURN RIGHT\a\b".encode())
        return Robot_move.RIGHT
    def SERVER_PICK_UP(self):
        self.request.sendall("105 GET MESSAGE\a\b".encode())
    def SERVER_LOGOUT(self):
        self.request.sendall("106 LOGOUT\a\b".encode())
    def SERVER_KEY_REQUEST(self):
        self.request.sendall("107 KEY REQUEST\a\b".encode())
    def SERVER_OK(self):
        self.request.sendall("200 OK\a\b".encode())
    def SERVER_LOGIN_FAILED(self):
        self.request.sendall("300 LOGIN FAILED\a\b".encode())
    def SERVER_SYNTAX_ERROR(self):
        self.request.sendall("301 SYNTAX ERROR\a\b".encode())
    def SERVER_LOGIC_ERROR(self):
        self.request.sendall("302 LOGIC ERROR\a\b".encode())
    def SERVER_KEY_OUT_OF_RANGE_ERROR(self):
        self.request.sendall("303 KEY OUT OF RANGE\a\b".encode())
class ThreadedTCPServer(socketserver.ThreadingMixIn,socketserver.TCPServer): pass



if __name__ == "__main__":
    # instantiate the server, and bind to localhost on port 9999
    server = ThreadedTCPServer((HOST, PORT), MyTCPSocketHandler)

    server.timeout = TIMEOUT
    while True:
        server.handle_request()